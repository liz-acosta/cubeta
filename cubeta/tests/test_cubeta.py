import ntpath

import pytest
from moto import mock_s3
import boto3
import os

CUBETA_NOMBRE = "test-bucket-name"
SECRETO_ACCESO_LLAVE = "test-secret-access-key"
ACCESO_LLAVE_ID = "test-access_key_id"
LLAVES_Y_NOMBRES_DEL_ARCHIVO = [
    {
        "key": "test_here/is/a/key",
        "filename": "fixtures/woman_yelling_at_cat_meme.jpg",
    },
    {
        "key": "test_here/is/another/key",
        "filename": "fixtures/star_wars_opening_crawl.xml",
    },
    {"key": "test_and/here/is/yet/another/key", "filename": ""},
]


@mock_s3
def create_s3_resource():
    s3_resource = boto3.resource("s3")
    bucket = s3_resource.create_bucket(Bucket=CUBETA_NOMBRE)
    return s3_resource, bucket


@mock_s3
def populate_test_bucket():
    s3_resource, bucket = create_s3_resource()
    from cubeta.cubeta import CubetaUtils

    cubeta_utls = CubetaUtils()

    cubeta_utls.upload_with_filename(LLAVES_Y_NOMBRES_DEL_ARCHIVO, CUBETA_NOMBRE)

    return populate_test_bucket


class TestCubetaBucket:
    @mock_s3
    def test_cubeta_bucket(self):
        populate_test_bucket()
        from cubeta.cubeta import CubetaBucket

        cubeta_bucket_object = CubetaBucket(
            bucket_name=CUBETA_NOMBRE,
            secret_access_key=SECRETO_ACCESO_LLAVE,
            access_key_id=ACCESO_LLAVE_ID,
        )

        assert cubeta_bucket_object.bucket.name == CUBETA_NOMBRE
        assert cubeta_bucket_object.bucket_dictionary == {
            "test_here/is/a/key": "/woman_yelling_at_cat_meme.jpg",
            "test_here/is/another/key": "/star_wars_opening_crawl.xml",
        }
        assert cubeta_bucket_object.s3_urls == [
            "s3://test-bucket-name/test_here/is/a/key/woman_yelling_at_cat_meme.jpg",
            "s3://test-bucket-name/test_here/is/another/key/star_wars_opening_crawl.xml",
        ]


class TestCubetaUtils:
    @mock_s3
    def test_cubeta_utils(self):
        create_s3_resource()
        from cubeta.cubeta import CubetaUtils

        cubeta_utils = CubetaUtils(
            secret_access_key=SECRETO_ACCESO_LLAVE, access_key_id=ACCESO_LLAVE_ID
        )

        cubeta_utils.upload_with_filename(
            LLAVES_Y_NOMBRES_DEL_ARCHIVO, "test-bucket-name"
        )

        assert (
            len(
                cubeta_utils.s3_resource.meta.client.list_objects(
                    Bucket="test-bucket-name"
                )["Contents"]
            )
            == 2
        )

        result = cubeta_utils.parse_s3_url_into_bucket_object(
            "s3://test-bucket-name/test_here/is/a/key/woman_yelling_at_cat_meme.jpg"
        )

        assert result.key == "test_here/is/a/key"
        assert result.metadata["filename"] == "woman_yelling_at_cat_meme.jpg"

        with cubeta_utils.save_object_to_temp_file(
            s3_url="s3://test-bucket-name/test_here/is/a/key/woman_yelling_at_cat_meme.jpg",
            prefix="tmp",
        ) as temp:
            absolute_path = os.path.abspath(f"{temp.name}")
            assert os.path.isfile(absolute_path) is True
            with open(absolute_path) as f:
                assert f

        assert os.path.isfile(absolute_path) is False
        with pytest.raises(FileNotFoundError):
            open(absolute_path)
