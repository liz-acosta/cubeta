import boto3
import os
import tempfile
from contextlib import contextmanager

import os.path
import os.path
from typing import List


class CubetaBucket:
    def __init__(self, bucket_name: str, secret_access_key, access_key_id):
        self.secret_access_key = os.environ.get(
            "SECRET_ACCESS_KEY", secret_access_key)
        self.access_key_id = os.environ.get("ACCESS_KEY_ID", access_key_id)
        self.bucket_name = bucket_name

    @property
    def s3_resource(self):
        s3_resource = boto3.resource(
            "s3",
            aws_access_key_id=self.access_key_id,
            aws_secret_access_key=self.secret_access_key,
        )
        return s3_resource

    @property
    def bucket(self):
        s3_bucket = self.s3_resource.Bucket(self.bucket_name)
        return s3_bucket

    def __format_filename(self, filename: str):
        formatted_filename = filename.split("/")[-1]
        return f"/{formatted_filename}"

    @property
    def bucket_dictionary(self):
        client = self.s3_resource.meta.client
        objects_response = client.list_objects(Bucket=self.bucket_name)

        dict = {
            content["Key"]: self.__format_filename(content['Key'])

            for content in objects_response['Contents']
        }
        return dict

    @property
    def s3_urls(self):
        urls = [
            f"s3://{self.bucket_name}/{filepath}{filename}"
            for filepath, filename in self.bucket_dictionary.items()
        ]
        return urls


class CubetaUtils:
    def __init__(self, secret_access_key, access_key_id):
        self.secret_access_key = os.environ.get("SECRET_ACCESS_KEY", secret_access_key)
        self.access_key_id = os.environ.get("ACCESS_KEY_ID", access_key_id)

    @property
    def s3_resource(self):
        s3_resource = boto3.resource(
            "s3",
            aws_access_key_id=self.access_key_id,
            aws_secret_access_key=self.secret_access_key,
        )
        return s3_resource

    def upload_with_filename(self, keys_and_filenames: List[dict], bucket_name):
        for item in keys_and_filenames:
            if item["filename"] is not "":
                self.s3_resource.meta.client.put_object(
                    Bucket=bucket_name,
                    Key=f"{item['key']}/{item['filename']}",
                    Metadata={"filename": f"{item['filename']}"},
                )

    def parse_s3_url_into_bucket_object(self, s3_url: str):
        s3_url_as_list = s3_url.replace("s3://", "").split("/", 1)
        bucket = s3_url_as_list[0]
        key = s3_url_as_list[-1]

        bucket_object = self.s3_resource.Object(bucket_name=bucket, key=f"{key}")

        return bucket_object

    @contextmanager
    def save_object_to_temp_file(self, s3_url: str, prefix=""):
        bucket = self.parse_s3_url_into_bucket_object(s3_url)
        temp_file = tempfile.NamedTemporaryFile(suffix=prefix)
        try:
            self.s3_resource.meta.client.download_fileobj(
                bucket.bucket_name, bucket.key, temp_file
            )
            yield temp_file
        finally:
            if os.path.exists(f"{temp_file.name}"):
                temp_file.close()
            else:
                print(f"Temporary file{temp_file.name} successfully removed")
